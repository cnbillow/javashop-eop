Javashop-eop是一套开源的快速开发框架，可以快速搭建web项目。<br>
目前已经比如成熟的是网店系统：javashop：[www.javamall.com.cn](http://www.javamall.com.cn) <br>
本框架基于spring、spring-mvc、spring-jdbc、freemarker搭建。<br>

包含web站点的基础功能：<br>
1. 站点安装（基础数据库的安装和安装锁定）<br>
2. 前台模板解析（[遵循eop的模板引擎规则](http://www.javamall.com.cn/version4/docs/template_help.html)）<br>
3. 插件（组件）机制（遵循eop的[组件机制规则](http://www.javamall.com.cn/help/index.php/组件开发指南)）<br>
4. 站点后台基础功能：后台登陆、后面界面展示、后台菜单管理<br>
管理员及权限管理、广告管理等<br>

[快速上手戳这里](https://git.oschina.net/javashopeop/javashop-eop/wikis/javashop-eop框架快速上手)<br>
[javashop eop基础功能介绍](https://git.oschina.net/javashopeop/javashop-eop/wikis/javashop-eop基础功能介绍)<br>

同时该开源项目长期招募开发者，欢迎大家一起完善这个开源框架，为中国的开源事业做出一点贡献！<br>
如有意向加入的同学可在OSChina上申请，或在下面的交流群中申请。<br>
 
**视频教程**<br>
[架构简介](http://v.youku.com/v_show/id_XMTM0MTkzOTAwNA==.html)<br>
[模板制作指南](http://v.youku.com/v_show/id_XMTM0MTk1NDc5Mg==.html)<br>
[组件机制1](http://v.youku.com/v_show/id_XMTM0MTk1NDc5Mg==.html)<br>
[组件机制2](http://v.youku.com/v_show/id_XMTM0MTk1Mzg2MA==.html)<br>
[上下文管理和数据库操作](http://v.youku.com/v_show/id_XMTM0MTk1MzY3Ng==.html)<br>


欢迎加入Javashop-eop框架的技术交流群：28755304<br>
或进入[Javashop-eop框架的技术交流论坛版块](http://www.javamall.com.cn/forum/forum.php?mod=forumdisplay&fid=11)交流